package deprecated

/**
 * Created by leonhard on 25.07.17.
 */
import com.google.gson.JsonObject
import org.junit.Test
import org.musicmanager.config.Config

class ConfigTest {

    @Test fun simpleValues() {
        val config = FileConfig()
        assert(config.testString == "hi")
        assert(config.testInt == 1)
        assert(config.testBoolean == true)
    }

    @Test fun setSimpleValues() {
        val newInt = 2
        val newString = "bye"
        val newBoolean = false


        safeTest({
            FileConfig()
        }, { config ->
            config.testInt = newInt
            config.testString = newString
            config.testBoolean = newBoolean
        }, { config ->
            assert(config.testInt == newInt)
            assert(config.testString == newString)
            assert(config.testBoolean == newBoolean)

            with(config.reader()) {
                assert(get("testString").asString == newString)
                assert(get("testInt").asInt == newInt)
                assert(get("testBoolean").asBoolean == newBoolean)
            }
        }, { config ->
            config.defaultValues()
        })

    }

    @Test fun deepValues(){
        val SimpleConfig = FileConfig()

        assert(SimpleConfig.deepString == "deep")
        assert(SimpleConfig.deep.get("deepString").asString == "deep")

        assert(SimpleConfig.deepInt ==  3)
        assert(SimpleConfig.deep.get("deepInt").asInt == 3)

        assert(SimpleConfig.deepBoolean == true)
        assert(SimpleConfig.deep.get("deepdeep").asBoolean == true)

    }
    @Test fun setDeepValues(){
        val newDeepString = "notDeep"
        val newDeepInt = 4
        val newDeepBoolean = false

        safeTest({
            FileConfig()
        }, { config ->
            config.deepString = newDeepString
            config.deepInt = newDeepInt
            config.deepBoolean = newDeepBoolean
        }, { config ->
            assert(config.deepString == newDeepString)
            assert(config.deepInt == newDeepInt)
            assert(config.deepBoolean == newDeepBoolean)

            with(config.reader().get("deep").asJsonObject){
                assert(get("deepString").asString == newDeepString)
                assert(get("deepInt").asInt == newDeepInt)
                assert(get("deepdeep").asBoolean == newDeepBoolean)
            }
        }, { config ->
            config.defaultValues()
        })
    }

//    @Test fun arrayTest() {
//        val constantValue = "blub"
//        val testValue = "flub"
//
//        safeTest({
//            deprecated.FileConfig()
//        },{ config ->
//            config.testStringSet.add(testValue)
//            assert( ! config.testStringSet.add(constantValue))
//        },{ config ->
//            assert(config.testStringSet.contains(testValue))
//            assert(config.testStringSet.contains(testValue))
//        },{ config ->
//            config.testStringSet.remove(testValue)
//        })
//    }

    fun <T: Config> safeTest(supplier: () -> T, action: (T) -> Unit, test: (T) -> Unit, cleanup: (T) -> Unit){
        val config = supplier()

        action(config)
        test(config)
        test(supplier())

        cleanup(config)
    }


}

class FileConfig : Config("simpleConf.json"){
    var testString by string(default = "hi")
    var testInt by int(1)
    var testBoolean by boolean(true)

    val deep by jsonObject(default = JsonObject())
    var deepString by string(default = "deep", jsonObject = deep)
    var deepInt by int(default = 3, jsonObject = deep)
    var deepBoolean by boolean(default = true, jsonObject = deep, jsonName = "deepdeep")

    val testStringSet by stringSet()


    var configArray by jsonArray()
    var configObject by jsonObject()

    fun defaultValues(){
        testString = "hi"
        testInt = 1
        testBoolean = true

        deepString = "deep"
        deepInt = 3
        deepBoolean = true

        /*deprecated.FileConfig::class.members.filter { it is KProperty0 }.map {
        val property = it as KProperty0
        val delegate = property.getDelegate() as JsonProperty<*>
        return@map property to delegate
    }.forEach {
        it.second.setValue(this, it.first, it.second.default)
    }
    */

    }

}





