import org.artrax.ArtistRole.*
import org.junit.Test

class SQLTests{

    @Test
    fun ArtistRolesSorting(){
        assert(values().sorted() == listOf(PRODUCER, PERFORMER, FEATURED, REMIXER))
    }
}