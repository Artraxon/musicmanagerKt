package deprecated

import com.google.common.collect.HashBasedTable
import com.mpatric.mp3agic.ID3v2
import org.artrax.config.Config
import org.musicmanager.element.TagType.*
import kotlin.reflect.KClass

/**
 * Created by leonhard on 28.06.17.
 */
data class Tag protected constructor(val label: String, val type: TagType) {


    companion object{
        private val tagProcessors: HashBasedTable<Tag, in KClass<Element>, TagProcessor<*>> = HashBasedTable.create()

        fun HashBasedTable<Tag, in KClass<Element>, TagProcessor<*>>.getByLabel(label: String, tagType: TagType)
                = this.rowMap().entries.find { it.key.label == label && it.key.type == tagType }?.value!!

        fun HashBasedTable<Tag, in KClass<Element>, TagProcessor<*>>.getTagByLabel(label: String, tagType: TagType): Tag {
            return rowKeySet().find { it.label == label && it.type == tagType}!!
        }

        fun <T: Element> addTagProcessor(tag: Tag, type: KClass<T>, tagProcessor: TagProcessor<T>)
                = tagProcessors.put(tag, type as KClass<Element>, tagProcessor)

        fun <T: Element> addTagProcessor(label: String, tagType: TagType, type: KClass<T>, tagProcessor: TagProcessor<T>)
                = addTagProcessor(getTagByLabel(label, tagType), type, tagProcessor)

        fun <T: Element> getTagProcessor(tag: Tag, type: KClass<in T>)
                = getTagTable().get(tag, type)!! as TagProcessor<T>

        fun <T: Element> getTagProcessor(label: String, tagType: TagType, type: KClass<T>): TagProcessor<T> {
            val tagtable = getTagTable()
            return tagtable.getByLabel(label, tagType).get(type) as TagProcessor<T>
        }

        fun getTagByLabel(label: String, tagType: TagType) = getTagTable().getTagByLabel(label, tagType)

        fun getTagTable() = HashBasedTable.create(tagProcessors)
    }

    /**
     * Each source should make sure that for every remote tag there is an registered TagProcessor
     * @property T the Type of Element this TagProcessor is responsible for
     */
    abstract class TagProcessor<T: Element>{

        /**
         * Is called when the Tag this was created for is added to a [T]
         */
        abstract fun <T> add(element: T)

        /**
         * Is called when the Tag this was created for is removed from a [T]
         */
        abstract fun <T> remove(element: T)
    }
}

/**
 * Created by leonhard on 18.07.17.
 * Contains general info about how tags should be saved in the mp3 File
 * @see ID3v2
 */

object TagEncoding: Config("TagEncoding.json"){

    /**
     * the ID3v2 Tag which should contain the leading performer if the Song is not a remix.
     * If it is, it will contain both the original Artist and the remixer
     */
    val artist by string()


    /**
     * The ID3v2 Tag which should contain the original artist (the leading performer) if the song is a remix
     */
    val originalArtist by string()

    /**
     * The ID3v2 Tag which should contain the remixer in case the song is a remix
     */
    val remixArtist by string()

    /**
     * The ID3v2 Tag which should contain the featured Artists
     */
    val featuredArtist by string()

    /**
     * The ID3v2 Tag which should contain the origin of the video. If empty, origin will only be stored in the database
     */
    val origin by string()

    /**
     * A sequence which will be used to delimeter multiple entries in one tag.
     * E.g. if set to "//" and a song is by Peter and Patrick, the artist field would contain "Peter//Patrick"
     */
    val tagDelimeter by string()


    /**
     * stores for every [TagType] how the ID3v2 Tags can be set or retrieved
     */
    val typeToField: Map<TagType, Pair<ID3v2.() -> String, ID3v2.(String) -> Unit>>

    init {
        fun selectField(encoding: String): Pair<ID3v2.() -> String, ID3v2.(String) -> Unit> {
            return when (encoding){
                "TCOM" -> ID3v2::getComposer to ID3v2::setComposer
                "TOPE" -> ID3v2::getOriginalArtist to ID3v2::setOriginalArtist
                "TPE1" -> ID3v2::getArtist to ID3v2::setArtist
                "TPE2" -> ID3v2::getAlbumArtist to ID3v2::setAlbumArtist
                "TPUB" -> ID3v2::getPublisher to ID3v2::setPublisher
                "COMM" -> ID3v2::getComment to ID3v2::setComment
                else -> throw InvalidTagEncodingException(encoding)
            }
        }
        typeToField = mapOf(
                ARTIST to selectField(artist),
                FEATURED to selectField(featuredArtist),
                ORIGINAL to selectField(originalArtist),
                REMIXER to selectField(remixArtist),
                ORIGIN to try {
                 selectField(origin)
                }catch (e: InvalidTagEncodingException){
                    (fun ID3v2.(): String {
                        return ""
                    }) to fun ID3v2.(value: String){}
                },
                GENRE to  (
                        fun ID3v2.(): String {return genre.toString()}
                                to fun ID3v2.(value: String){genre = value.toInt()}
                        ),
                GROUP to (
                        ID3v2::getGrouping
                                to ID3v2::setGrouping
                        )


        )
    }
}

/**
 * Thrown when a field of [TagEncoding] can't be translated into a ID3v2 tag
 */
class InvalidTagEncodingException(val encoding: String) : Exception("\'$encoding is an invalid encoding for a tag")

/**
 * Overwrites an ID3v2 Tag for the given [TagType] with the given value according to [TagEncoding.typeToField]
 * [addTag] should be the preferred method
 */
fun ID3v2.setTag(type: TagType, value: String) {
    TagEncoding.typeToField.get(type)!!.second.invoke(this, value)
}

/**
 * Retrieves an ID3v2 tag according to [TagEncoding.typeToField]
 */
fun ID3v2.getTag(type: TagType): String{
    return run(TagEncoding.typeToField.get(type)!!.first)
}


/**
 * Adds a Tag to the mp3 file
 * @throws InvalidArtistType if the song is a remix and [ARTIST] is tried to be set
 */
fun ID3v2.addTag(type: TagType, value: String){

    fun assemble(first: String, second: String): String{
        return if(first.isEmpty()){
            first
        } else {
            first + TagEncoding.tagDelimeter
        }.plus(second)
    }

    if(type == ARTIST && isRemix()) throw InvalidArtistType(type, this)

    setTag(type, assemble(getTag(type), value))
}

fun ID3v2.addTag(type: TagType, vararg value: String) = value.forEach { addTag(type, it) }

fun ID3v2.removeTag(type: TagType, value: String){
    setTag(type, run(TagEncoding.typeToField.get(type)!!.first).split(TagEncoding.tagDelimeter)
            .filter{ it != value }
            .joinToString(TagEncoding.tagDelimeter))
}

fun ID3v2.removeTag(type: TagType, vararg value: String) = value.forEach { removeTag(type, it) }

fun ID3v2.getTags(type: TagType): Set<String>{
    return getTag(type).split(TagEncoding.tagDelimeter).toSet()
}

fun ID3v2.removeTag(type: TagType) = setTag(type, "")

/**
 * Indicates that [type] cannot be set or retrieved
 */
class InvalidArtistType(val type: TagType, val tag: ID3v2? = null): Exception()

fun ID3v2.isRemix() = !(artist.isNullOrEmpty() || originalArtist.isNullOrEmpty())

enum class TagType {
    ARTIST, ORIGINAL, REMIXER, FEATURED, GENRE, GROUP, TITLE, ORIGIN,

    OTHER
}
