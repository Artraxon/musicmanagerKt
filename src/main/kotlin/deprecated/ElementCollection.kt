package deprecated

import com.google.gson.JsonObject
import org.artrax.config.Config
import java.net.URI
import kotlin.reflect.KClass

/**
 * Created by leonhard on 06.06.17.
 */

abstract class ElementCollection<T: Element>(val label: String, val inputuri: URI,
                                             val data: InputInfo, elementListInfoLazy : Lazy<CollectionElements>, val type: KClass<T>){

    abstract val tag: Tag


    constructor(data: InputInfo, elementListInfoLazy: Lazy<CollectionElements>, type: KClass<T>) :
            this(data.label, URI(data.uri), data, elementListInfoLazy, type)

    val tagProcesor
        get() = Tag.getTagProcessor(tag, type)

    val elementListInfo by elementListInfoLazy
    val elements by lazy { loadElements().toMutableSet() }
    val primaryElements get() = elements.filter { it.info.input == this }
    val secondaryElements get() =  elements.filter { it.info.input != this }

    fun getElementsCopy() = elements.toSet()


    /**
     * Synchronizes the DB with the remote, so the DB is conform with the remote
     */
    protected abstract fun syncRemoteToDB()

    /**
     * Synchronizes the remote with the db, so remote is conform with the db
     */
    protected abstract fun syncDBToRemote()

    protected abstract fun loadElements(): Set<T>

    /**
     * Removes an Element from the Collection
     */
    protected abstract fun removeElement(element: T)

    /**
     * Adds an Element to the Collection
     */
    protected abstract fun addElement(element: T)

    abstract fun createElement(any: Any): Element.ElementInfo<T>

    /**
     * If the element is not already present in the collection, it is added
     * @return the created element
     */
    abstract fun loadElement(info: Element.ElementInfo<T>): T

    /**
     * Contains information about this [ElementCollection]
     * @property label the label of this instance.
     * @property tag string representation of the tag this instance is representating
     * @property uri the (remote) origin of this instance
     * @property elementListLocation where the [CollectionElements] can be found
     */
    abstract class InputInfo(parent: Config, identifier: (JsonObject) -> Boolean, arrayName: String = "inputs")
        : Config(){

        val label by string()
        val tag by string()
        val uri by string()
        val elementListLocation by string()
    }

    /**
     * Contains Information about the elements of this collection
     * @property elements the elements of this collection
     */
    abstract class CollectionElements(path: String): Config(path){
        val elements by set<JsonObject>()
    }
}

