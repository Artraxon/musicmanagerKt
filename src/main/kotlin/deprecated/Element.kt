package deprecated

import com.mpatric.mp3agic.ID3v24Tag
import com.mpatric.mp3agic.Mp3File
import org.artrax.config.Config
import org.artrax.utility.CustomMutableSet
import deprecated.TagType.*
import java.io.File

/**
 * Created by leonhard on 06.06.17.
 */

abstract class Element(val info: ElementInfo<*>, val remoteTags: Set<TagType>) {

    val file
        get() = File(info.storage_location)

    private val mp3file
        get() = Mp3File(file)

    protected fun <T> withTag(action: ID3v24Tag.() -> T): T{
        val tag = ID3v24Tag(mp3file.id3v2Tag.toBytes())
        val result = tag.action()
        mp3file.id3v2Tag = tag

        mp3file.save(file.path)
        return result
    }

    val internal_id = info.internal_id
    val fullname get() = info.fullname

    val tags
        get() = values().associate { it to TagList({info.getTag(it)}, it) }


    val artists
        get() = tags.get(ARTIST)!!

    val originalArtists
        get() = tags.get(ORIGINAL)!!

    val featuredArtists
        get() = tags.get(FEATURED)!!

    val remixArtists
        get() = tags.get(REMIXER)!!

    val genres
        get() = tags.get(GENRE)!!

    val groupings
        get() = tags.get(GROUP)!!

    var origin
        get() = tags.get(ORIGIN)!!.first()
        set(value) = tags.get(ORIGIN)!!.run {
            clear()
            add(value)
        }

    var title
        get() = tags.get(TITLE)!!.first()
        set (value) = tags.get(TITLE)!!.run {
            clear()
            add(value)
        }


    open val fileIsSync: Boolean get() {
        return values().all {
            tagIsSync(it)
        }
    }

    fun tagIsSync(type: TagType,
                  checkTo: String = info.getTag(type).joinToString(separator = TagEncoding.tagDelimeter)): Boolean{
                return withTag {
                    getTag(type) == checkTo
                }
    }

    /**
     * Modifies the file so it is synchronized with the Database
     */
    open fun syncFileFromDB(){
        values().filterNot { tagIsSync(it) }.forEach {
            withTag { 
                removeTag(it)
                addTag(it, *info.getTag(it).toTypedArray())
            }            
        }
    }

    /**
     * Modifies the Database so it is synchronizes with the Database
     */
    open fun syncFileToDB(){
        values().filterNot { tagIsSync(it) }.forEach {
            val tags = info.getTag(it)
            tags.clear()
            tags.addAll(withTag {
                getTags(it)
            })
        }
    }


    /**
     * Updates File, JSON and [tags] from [update]
     */
    fun updateLocalFromInfo(update : ElementInfo<*>, reset: Boolean = true) {
        TagType.values().forEach { tagType: TagType ->
            val originalTag = info.getTag(tagType)
            val newTag = update.getTag(tagType)

            val toRemove = originalTag.filter { !newTag.contains(it) }
            val toAdd = newTag.filter { !originalTag.contains(it) }

            toRemove.forEach {
                Tag.getTagProcessor(it, tagType, this::class).remove(this)
                tags.get(tagType)!!.remove(it)
            }

            toAdd.forEach {
                Tag.getTagProcessor(it, tagType, this::class).add(this)
                tags.get(tagType)!!.remove(it)
            }
        }
    }

    abstract fun syncRemoteToDB()
    abstract fun syncDBToRemote()

    abstract class ElementInfo<T: Element> : Config {

        val input: ElementCollection<T>

        constructor(input: ElementCollection<T>, internal_id: String) :
                super(input.elementListInfo, ElementCollection.CollectionElements::elements, { it -> it.get("internal_id").asString == internal_id }) {

            this.input = input
            this.internal_id = internal_id

        }

        constructor(input: ElementCollection<T>, init: ElementInfo<T>.() -> Unit) : super(){
            this.input = input
            init()
            initDefaults()
        }

        open val tags by set<String>()
        open val genre by set<String>()
        open val fullname by string()
        open var title by string()
        open var artist by set<String>()
        open var featured_Artist by set<String>()
        open var original_Artist by set<String>()
        open var remix_Artist by set<String>()
        open val origin by set<String>()
        open var storage_location by string()
        open var internal_id by string()


        fun getTag(type: TagType): MutableSet<String>{
            return when(type){
                ORIGINAL -> original_Artist
                REMIXER -> remix_Artist
                ARTIST -> artist
                GENRE -> genre
                FEATURED -> featured_Artist
                GROUP -> tags
                TITLE -> mutableSetOf(title)
                ORIGIN -> origin
            }
        }

    }

    /**
     * Stores entries for a specific tag,
     * all operations are reflected on the json and the mp3file
     */
    inner class TagList(delegate: () -> MutableSet<String>, type: TagType):
            CustomMutableSet<String>(delegate(),
                    {element: String ->
                        info.getTag(type).contains(element).not().also {
                            if(it) {
                                info.getTag(type).add(element)
                                withTag {
                                    addTag(type, element)
                                }
                            }
                        }
                    },
                    {element: String ->
                        info.getTag(type).contains(element).also {
                            if(it) {
                                info.getTag(type).remove(element)
                                withTag {
                                    removeTag(type, element)
                                }
                            }
                        }
                    })
}



