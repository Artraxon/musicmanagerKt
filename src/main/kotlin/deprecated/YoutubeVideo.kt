package deprecated

import com.google.api.services.youtube.model.Video
import org.artrax.config.Config
import deprecated.TagType.ORIGINAL
import deprecated.TagType.REMIXER
import org.musicmanager.youtube.TagWhitelist.tags
import org.musicmanager.youtube.YoutubeParsing.originalArtist
import org.musicmanager.youtube.YoutubeParsing.regex
import org.musicmanager.youtube.YoutubeParsing.remixArtist
import org.musicmanager.youtube.YoutubeParsing.splits
import org.musicmanager.youtube.YoutubeParsing.title


/**
 * Created by leonhard on 17.06.17.
 */

class YoutubeVideo(info: YoutubeVideoInfo, remoteTags: Set<TagType>) : Element(info, remoteTags) {

    val remoteVideo: Video get() = videoFromId(info.internal_id)


    override fun syncRemoteToDB() {
        updateLocalFromInfo((info.input as YTPlaylist).createElement(remoteVideo))
    }


    override fun syncDBToRemote() {
        //TODO
    }

    open class YoutubeVideoInfo(yt_ID: String, val list: YTPlaylist):
            ElementInfo<YoutubeVideo>(list, yt_ID , "videos") {

        open val uploader by string()
    }

}


/**
 * Tries to retrieve the title, the artists and other tags with the help of the Regex from [YoutubeParsing], and
 * the tags from the snippet from [video]
 */
fun detectInfo(video: Video): ParsedVideoInfo {
    val regex = Regex(YoutubeParsing.regex)
    regex.find(video.snippet.title)?.let { match ->

        fun findMatch(positions: Set<Int>): List<String> {
            return positions.filter { match.groups.get(it) != null }.map {
                    match.groups.get(it)!!.value
                }
        }

        val artists = mutableMapOf(
                ORIGINAL to findMatch(YoutubeParsing.originalArtist),
                REMIXER to findMatch(YoutubeParsing.remixArtist)).
                mapValues { it.value.flatMap { it.split(Regex(YoutubeParsing.splits))}.toSet() }

        val title = findMatch(YoutubeParsing.title).first()

        val allTags = match.groupValues.plus(video.snippet.tags).filter { TagWhitelist.tags.contains(it) }.toSet()

        val (tags, genres) = allTags.groupBy { TagWhitelist.genres.contains(it) }.run {
            get(false)!!.toSet() to get(true)!!.toSet()
        }

        return ParsedVideoInfo(artists, title, tags, genres)
    } ?: throw FailedParseException(video, regex)

}

/**
 * Thrown when [detectInfo] wasn't able to match the title of the video with the regex
 * @property video The [Video] which couldn't be parsed
 * @property regex The used [Regex]
 */
class FailedParseException(val video: Video, val regex: Regex) : Exception("failed to retrieve any informations from the video")

/**
 * @property artists The artists which participated in the video
 * @property title the Title of the song
 * @property tags other tags about this video
 */
data class ParsedVideoInfo(val artists: Map<TagType, Set<String>>, val title: String, val tags: Set<String>, val genres: Set<String>)

/**
 * Contains information on how to retrieve information from [Video]
 * @property regex the remix describing how the original title of the video is build
 * @property title the groups containing the
 * @property originalArtist the groups containing the original Artists
 * @property remixArtist the groups containing the remix Artists
 * @property splits a Regex describing how Strings containing Artists and Tags should be split
 */
object YoutubeParsing: Config("YoutubeParsing.json"){
    val regex by string()
    val title by set<Int>()
    val originalArtist by set<Int>()
    val remixArtist by set<Int>()
    val splits by string()
}

/**
 * @property tags the tags which are allowed
 */
object TagWhitelist: Config("TagWhitelist.json"){
    val tags by set<String>()
    val genres by set<String>()
}

fun String.defaultIfEmpty(default: String): String{
    return  if(isEmpty()) default else this
}
