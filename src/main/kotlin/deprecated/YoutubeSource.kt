package deprecated

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.auth.oauth2.StoredCredential
import com.google.api.client.extensions.java6.auth.oauth2.AuthorizationCodeInstalledApp
import com.google.api.client.extensions.jetty.auth.oauth2.LocalServerReceiver
import com.google.api.client.googleapis.auth.oauth2.GoogleAuthorizationCodeFlow
import com.google.api.client.googleapis.auth.oauth2.GoogleClientSecrets
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.util.store.FileDataStoreFactory
import com.google.api.services.youtube.YouTube
import com.google.api.services.youtube.model.PlaylistItem
import com.google.api.services.youtube.model.Video
import com.google.gson.JsonObject
import org.artrax.config.Config
import org.artrax.config.toJsonPrimitive
import deprecated.YTPlaylist.YoutubeInputInfo
import java.io.*


/**
 * Created by leonhard on 17.06.17.
 */

const val YT_VIDEO_LIST = "snippet"
class YoutubeSource : Source<YoutubeVideo>("Youtube"){

    override fun loadElementsFromDB(): MutableMap<Element, ElementCollection<YoutubeVideo>> {
        return inputs.flatMap { input -> input.primaryElements.associate { it to input  }.entries }.
                map { it.key to it.value }.
                let { mutableMapOf(*it.toTypedArray()) }
    }

    override fun loadInputsFromDB(): MutableSet<ElementCollection<YoutubeVideo>> {
        return YoutubeSourcesData.inputs.map {
            YTPlaylist(it,
                    if (TagWhitelist.genres.contains(it.tag.toJsonPrimitive())) TagType.GENRE
                    else TagType.GROUP)
        }.toMutableSet()
    }


    companion object {
        val JSON_FACTORY = JacksonFactory()
        val HTTP_TRANSPORT = NetHttpTransport()

        object YoutubeSourcesData : Config(Config.getPath("youtube_sources.json")){
            val inputsArray by set<JsonObject>()
            val inputs get() = inputsArray.
                    map { YoutubeInputInfo(it.asJsonObject.get("id").asString) }



        }


        object YoutubeConfig : Config(Config.getPath("youtube.json")) {
            val credential_datastore by string()
            val client_secret by string()
        }

        val youTube: YouTube

        init {
            val scopes = listOf("https://www.googleapis.com/auth/youtube")
            val clsreader = InputStreamReader(FileInputStream(YoutubeConfig.client_secret))
            val credential = auth(clsreader, scopes)
            youTube = YouTube.Builder(HTTP_TRANSPORT, JSON_FACTORY, credential).setApplicationName("music-manager").build()
            //TODO vernünftiges handlen der IO Exception von auth()
        }

        @Throws(IOException::class)
        fun auth(clientSecretReader: Reader, scopes: List<String>): Credential? {
            val credentialDatastore = YoutubeConfig.credential_datastore
            val clientSecrets = GoogleClientSecrets.load(JSON_FACTORY, clientSecretReader)

            val fileDataStoreFactory = FileDataStoreFactory(File(YoutubeConfig.credential_datastore))
            val dataStore = fileDataStoreFactory.getDataStore<StoredCredential>(credentialDatastore)

            val flow = GoogleAuthorizationCodeFlow.Builder(HTTP_TRANSPORT, JSON_FACTORY, clientSecrets, scopes).build()

            val localReceiver = LocalServerReceiver.Builder().setPort(8080).build()

            try {
                return AuthorizationCodeInstalledApp(flow, localReceiver).authorize("user")
            } catch (e: Exception) {
                e.printStackTrace()
                return null
            }

        }


    }


}

fun videoFromPlaylistItem(item: PlaylistItem): Video = videoFromId(item.contentDetails.videoId)

fun videoFromId(id: String): Video{
    return YoutubeSource.youTube.videos().list(YT_VIDEO_LIST).setId(id).execute().items.first()
}





