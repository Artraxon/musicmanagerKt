package deprecated


/**
 * Created by leonhard on 06.06.17.
 */

abstract class Source<T: Element>(val label: String) {


    init {
        addSource(label, this)
    }

    val elements by lazy { loadElementsFromDB() }
    val inputs by lazy { loadInputsFromDB() }


    protected abstract fun loadElementsFromDB(): MutableMap<Element, ElementCollection<T>>
    protected abstract fun loadInputsFromDB(): MutableSet<ElementCollection<T>>

    companion object {

        private val sources = hashMapOf<String, Source<Element>>()


        fun getSource(label: String) = sources.get(label)

        fun addSource(label: String, source: Source<out Element>) = sources.put(label, source as Source<Element>)
    }
}

