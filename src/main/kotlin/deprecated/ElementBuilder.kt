package deprecated

import com.google.gson.JsonArray
import com.google.gson.JsonObject


const private val original_Artist_field = "original"
const private val remix_Artist_field = "remixer"

/*open class ElementBuilder<T: Element> private constructor(){

    lateinit var fullname: String
    lateinit var internal_id: String
    lateinit var origin: String
    lateinit var collection: ElementCollection<T>

    var originalArtist: Set<String>? = null
    var remixArtist: Set<String>? = null

    var title: String? = null
    var storage_location: String? = null

    private var _tags: JsonArray? = null
    var tags: JsonArray
        get() {
            if(_tags == null){
                _tags = JsonArray()
            }
            if(_tags!!.size() == 0){
                _tags!!.also {
                    it.find { it.asString == collection.tag.label } ?: it.add(collection.tag.label)
                }
            }
            return _tags!!
        }
    set(value){
        _tags = value
    }



    constructor(init:  ElementBuilder<T>.() -> Unit) : this(){
        this.init()
    }

    fun build() = collection.loadElement(this)



    fun toJson(): JsonObject {
        val json = JsonObject()
        json.addProperty("fullname", fullname)
        json.addProperty("internal_id", internal_id)
        json.addProperty("origin", origin)

        originalArtist?.let { json.add(original_Artist_field, JsonArray().apply { it.forEach(this::add) }) }
        remixArtist?.let { json.add(remix_Artist_field, JsonArray().apply { it.forEach(this::add) }) }
        title?.let { json.addProperty("title", it) }
        storage_location?.let { json.addProperty("storage_location", it) }

        json.add("tags", tags)

        return json
    }

}
        */