package deprecated

import com.google.api.services.youtube.model.*
import org.musicmanager.element.*
import deprecated.YoutubeSource.Companion.YoutubeSourcesData
import deprecated.YoutubeSource.Companion.youTube

/**
 * Created by leonhard on 19.06.17.
 */
const val YT_REQUEST_LOAD_PARTS = "snippet"
const val YT_INSERT_PARTS = "snippet"
class YTPlaylist(data: YoutubeInputInfo, val tagType: TagType) :
        ElementCollection<YoutubeVideo>(data, lazy { YoutubeElements(data.elementListLocation) }, YoutubeVideo::class) {

    override val tag: Tag
        get() = Tag.getTagByLabel(data.tag, tagType)

    init {
        Tag.addTagProcessor(tag, YoutubeVideo::class, YoutubeTagProcessor<YoutubeVideo>())
    }

    override fun loadElements(): Set<YoutubeVideo> {
        return elementListInfo.elements.map {
            YoutubeVideo(YoutubeVideo.YoutubeVideoInfo(it.asJsonObject.get("internal_id").asString, this), setOf(ytplaylistTagType))
        }.toSet()
    }

    private val remotePlaylistItems: PlaylistItemListResponse
        get() = YoutubeSource.youTube.playlistItems().list(YT_REQUEST_LOAD_PARTS).
                setPlaylistId(inputuri.toString()).
                execute()

    override fun syncRemoteToDB() {
        val playlistResponse = remotePlaylistItems

        val toAdd = playlistResponse.items.
                filter { item ->
                    val element = elements.find { it.internal_id == item.contentDetails.videoId }
                    if(element != null){
                        element.syncRemoteToDB()
                        return@filter false
                    }else{
                        return@filter true
                    }}

        val toRemove = elements.filter { element -> playlistResponse.items.find { element.internal_id == it.contentDetails.videoId } == null }

        toRemove.forEach(this::removeElement)
        toAdd.map { createElement(it) }.forEach { loadElement(it) }

    }

    private fun removeRemoteElement(videoId: String) = youTube.playlistItems().delete(videoId).execute()

    private fun addRemoteElement(videoId: String): PlaylistItem {
        val toInsert = PlaylistItem()
        val snippet = PlaylistItemSnippet()
        val resourceID = ResourceId()

        resourceID.videoId = videoId
        resourceID.kind = "youtube#video"

        snippet.resourceId = resourceID
        toInsert.snippet = snippet

        return youTube.playlistItems().insert(YT_INSERT_PARTS, toInsert).execute()
    }


    override fun syncDBToRemote() {
        val remoteItems = remotePlaylistItems

        val toAdd = elements.
                filter { element ->
                    remoteItems.items.find { element.internal_id == it.contentDetails.videoId }.let {
                        if(it == null){
                            return@filter true
                        }else {
                            element.syncDBToRemote()
                            return@filter false
                        }
                    }}

        val toRemove = remoteItems.items.
                filter { item -> elements.find { item.contentDetails.videoId == it.internal_id} == null }

        toAdd.forEach { addRemoteElement(it.internal_id) }
        toRemove.forEach { removeRemoteElement(it.contentDetails.videoId) }
        
    }

    override fun removeElement(element: YoutubeVideo) = tagProcesor.remove(element)

    override fun addElement(element: YoutubeVideo) = tagProcesor.add(element)

    /**
     * Creates an [YoutubeVideo.YoutubeVideoInfo] from the parameter
     * @param any must be [PlaylistItem] or [Video]
     * @return [YoutubeVideo.YoutubeVideoInfo]
     */
    override fun createElement(any: Any): Element.ElementInfo<YoutubeVideo> {
        if(any is PlaylistItem) {
            return object : YoutubeVideo.YoutubeVideoInfo(any.contentDetails.videoId, this@YTPlaylist) {
                override var internal_id = any.contentDetails.videoId
                override val fullname = any.snippet.title
                override val origin = mutableSetOf(assemblyYoutube(internal_id))

                val video = videoFromPlaylistItem(any)
                val parsedInfo = detectInfo(video)
                override var title = parsedInfo.title

                override val genre = parsedInfo.genres.toMutableSet()
                override val tags: MutableSet<String> = parsedInfo.tags.toMutableSet()

            }
        }else if(any is Video){
            return object : YoutubeVideo.YoutubeVideoInfo(any.id, this@YTPlaylist){
                override var internal_id = any.id
                override val fullname = any.snippet.title
                override val origin = mutableSetOf(assemblyYoutube(internal_id))

                val parsedInfo = detectInfo(any)
                override var title = parsedInfo.title

                override val genre = parsedInfo.genres.toMutableSet()
                override val tags: MutableSet<String> = parsedInfo.tags.toMutableSet()

            }

        }else throw IllegalArgumentException(" not a valid source")
    }


    /**
     * If the element isn't present yet, it is loaded into this collection
     */
    override fun loadElement(info: Element.ElementInfo<YoutubeVideo>): YoutubeVideo {

        val internal_id = info.internal_id
        val preexisting = elements.find { it.internal_id == internal_id }

        //Creating JSON and inserting into JSON Document
        return preexisting ?: run {
            elementListInfo.elements.add(info.jsonCopy)

            //Creating and adding Youtube Video
            val youtubeVideo = YoutubeVideo(YoutubeVideo.YoutubeVideoInfo(internal_id, this), setOf(ytplaylistTagType))
            addElement(youtubeVideo)

            return@run youtubeVideo
        }
    }

    /**
     * Information about this [YTPlaylist]
     */
    class YoutubeInputInfo(id: String)
        : InputInfo(YoutubeSourcesData, { it -> it.get("id").asString == id })

    class YoutubeElements(path: String) : CollectionElements(path)

    inner class YoutubeTagProcessor<T: YoutubeVideo>: Tag.TagProcessor<YoutubeVideo>(){
        override fun <T> add(element: T) {
            if(elements.contains(element as YoutubeVideo)){
                elements.add(element as YoutubeVideo)
                with(elementListInfo.elements) {
                    add(element.info.jsonCopy)
                }
            }
        }

        override fun <T> remove(element: T) {
            elements.remove(element as YoutubeVideo)
            with(elementListInfo.elements){
                remove(find { it.asJsonObject.get("internal_id").asString == element.internal_id })
            }
        }

    }

}

fun assemblyYoutube(yt_id: String) = "youtube.com/watch?v=$yt_id"

