package org.artrax
import com.google.gson.JsonObject
import org.artrax.TagPlaces.*
import org.artrax.config.Config

object GConf : Config("${defaultConfLocations.detectedPath}/config.json"){
    val dbpath by string(default = "${defaultConfLocations.detectedPath}/db")
    val delimiter by string(default = ";" )

    val defaults by jsonObject()
    val default_ArtistRole by enum(ArtistRole::class.java, default = ArtistRole.PERFORMER, jsonObject = defaults)


    val taghandling by jsonObject(default = JsonObject())


}

object TagHandling : Config(GConf, "taghandling"){
    val allowNonStandardGenres by boolean(default = true)
    val moodTag by enum(TagPlaces::class.java, default = GROUPING)
    val otherTag by enum(TagPlaces::class.java, default = COMMENT)
    val mainArtists by set<ArtistRole>(default = setOf(ArtistRole.PERFORMER))

    val artistMappings by jsonObject(default = JsonObject())
}

object ArtistMappings : Config(TagHandling, "artistMappings"){
    val onlyProducer by set(default = setOf(ARTIST, COMPOSER, ORIGINAL_ARTIST))
    val onlyPerformer by set(default = setOf(ARTIST, COMPOSER, ORIGINAL_ARTIST))

    val performerAndProducer by jsonObject(default = JsonObject())
    val performer by set(default = setOf(ARTIST, ORIGINAL_ARTIST), jsonObject = performerAndProducer)
    val producer by set(default = setOf(COMPOSER, ORIGINAL_ARTIST), jsonObject = performerAndProducer)

    val featured by set(default = setOf(ALBUM_ARTIST, ORIGINAL_ARTIST))

    val whenRemixed by jsonObject(default = JsonObject())

    val remix = Remix

}

object Remix : Config(ArtistMappings, "whenRemixed"){
    val remixer by set(default = setOf(ARTIST, COMPOSER))

    val onlyProducer by set(default = setOf(ARTIST, COMPOSER, ORIGINAL_ARTIST))
    val onlyPerformer by set(default = setOf(ARTIST, COMPOSER, ORIGINAL_ARTIST))

    val performerAndProducer by jsonObject(default = JsonObject())
    val performer by set(default = setOf(ARTIST, ORIGINAL_ARTIST, COMPOSER), jsonObject = performerAndProducer)
    val producer by set(default = setOf(COMPOSER, ORIGINAL_ARTIST), jsonObject = performerAndProducer)

    val featured by set(default = setOf(ALBUM_ARTIST, ORIGINAL_ARTIST))

}

enum class TagPlaces {
    GROUPING, COMMENT, GENRE,
    ARTIST, COMPOSER, ALBUM_ARTIST, ORIGINAL_ARTIST, TEMPO, KEY;
}

/**
 * @return a map where all elements by both iterable are mapped to whether they
 * * are contained in both iterable (mapped to [Crossing.BOTH])
 * * are contained only in the receiver (mapped to [Crossing.RECEIVER])
 * * are contained only in the given iterable (mapped to [Crossing.CROSSED])
 */
inline fun <E> Iterable<E>.cross(iterable: Iterable<E>): Map<Crossing, List<E>>{
    return this.union(iterable).groupBy {
        if(this.contains(it).not()) return@groupBy Crossing.CROSSED
        if(iterable.contains(it).not()) return@groupBy Crossing.RECEIVER
        return@groupBy Crossing.BOTH
    }
}

/**
 * Indicates whether an element subject to [Iterable.cross] was in both, only the receiver or only the crossed iterable
 */
enum class Crossing {
    BOTH, RECEIVER, CROSSED
}

inline fun <E> Map<Crossing, List<E>>.crossAction(crossinline both: (E) -> Unit, crossinline receiver: (E) -> Unit, crossinline crossed: (E) -> Unit){
    this.forEach { crossing, list ->
        when (crossing){
            Crossing.BOTH -> list.forEach(both)
            Crossing.RECEIVER -> list.forEach(receiver)
            Crossing.CROSSED -> list.forEach(crossed)
        }
    }
}
