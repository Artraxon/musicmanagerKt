package org.artrax.sources
import org.artrax.Element
import org.artrax.SourceSongs
import org.artrax.collection.ElementCollection

abstract class Source(val table: SourceSongs){

    init {
        sourceTables = sourceTables.plus(this to table)
    }

    /**
     * Loads or creates an element from an internal id.
     * If the source is a remote one, this should also be an unique identifier for the linked platform,
     * like Youtube Video ID
     * @param iid The Internal ID of the org.artrax.Element.
     * This should be equivalent to the corresponding value of [SourceSongs.id]
     * @return The creates or found element
     */
    abstract fun fromIID(iid: String): Element

    /**
     * Loads or creates
     */
    abstract fun fromCollection(ciid: String): ElementCollection<out Source>

    abstract fun link(iid: String, element: Element): Boolean


    companion object {
        private var _sourceTables = mapOf<Source, SourceSongs>()
        var sourceTables
            get() = _sourceTables.toMap()
            private set(value) {
                _sourceTables = value
            }
    }
}