package org.artrax
import org.artrax.sources.FileSource
import org.jetbrains.exposed.dao.*
import org.jetbrains.exposed.sql.*
import org.jetbrains.exposed.sql.transactions.TransactionManager
import kotlin.reflect.KMutableProperty0


object Songs : IntIdTable() {
    val name = varchar("name", 64)
    val location = varchar("location", 512)
    val label = reference("publisher", Publishers).nullable()
}

class Song(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Song>(Songs)

    var location by Songs.location
    val artistsWithRoles by ArtistSong referrersOn Songs.id
    val artists by Artist via ArtistsSongs
    var name by Songs.name
    var tags by Tag via SongTags
    var publisher by Publisher optionalReferencedOn Songs.label

    var collections by CollectionEntity via CollectionSongs
    val inCollectionsWithPosition by SongInCollectionEntity referrersOn Songs.id
}

object Publishers : IdTable<String>("publishers") {
    override val id: Column<EntityID<String>> = varchar("name", 64).primaryKey().entityId()
    val url = varchar("url", 128).nullable()
}

class Publisher(id: EntityID<String>): Entity<String>(id){
    companion object : EntityClass<String, Publisher>(Publishers){
        fun getOrNew(name: String, url: String? = null): Publisher {
            return Publisher.findById(name)
                    ?: new(name) {
                this.url = url
            }
        }
    }

    var name by Publishers.id
    var url by Publishers.url

    val songs by Song referrersOn Publishers.id
}

object AllSources : IntIdTable(){
    val name = varchar("source_name", 32)
}

class SourceEntity(id: EntityID<Int>) : IntEntity(id){
    companion object : IntEntityClass<SourceEntity>(AllSources)
    val name by AllSources.name
}

object SourceSongs : IntIdTable(){
    val songID = reference("songID", Songs)
    val internalID = varchar("internal_id", 512)
    val source_ref = reference("source", AllSources)
}

class SourceSongEntity(id: EntityID<Int>) : IntEntity(id){
    val song by Song referencedOn SourceSongs.songID
    val internal_ID by SourceSongs.internalID
    val source_ref by SourceSongs.source_ref
}

object CollectionTable : IntIdTable(){
    val tag = reference("tag", Tags)
    val ofSource = reference("source", AllSources)
    val uri = varchar("uri", 512)

}

class CollectionEntity(id: EntityID<Int>) : IntEntity(id){
    companion object : IntEntityClass<CollectionEntity>(CollectionTable)

    var tag by CollectionTable.tag
    var source by CollectionTable.ofSource
    var uri by CollectionTable.uri
    var songs by Song via CollectionSongs



}

object CollectionSongs : IntIdTable(){
    var song = reference("song", SourceSongs)
    var collection = reference("collection", CollectionTable)
    var position = integer("position")
}

class SongInCollectionEntity(id: EntityID<Int>) : IntEntity(id){
    companion object : IntEntityClass<SongInCollectionEntity>(CollectionSongs)

    val song by Song referencedOn CollectionSongs.song
    val position by CollectionSongs.position
    val collection by CollectionEntity referencedOn CollectionSongs.collection
}

object Artists : IntIdTable() {
    val name = varchar("name", 20)
}

class Artist(id: EntityID<Int>) : IntEntity(id){
    companion object : IntEntityClass<Artist>(Artists)

    var name by Artists.name
    val songsWithRoles by ArtistSong referrersOn ArtistsSongs.artist_ID
    val songs by Song via ArtistsSongs

}
enum class ArtistRole() {
    //Order should not be changed
    PRODUCER,
    PERFORMER, FEATURED, REMIXER, COMPOSER
}

object ArtistsSongs : IntIdTable() {
    val songID = reference("songID", Songs)
    val artist_ID = reference("artistID", Artists)
    val role = enumeration("role", ArtistRole::class.java).default(ArtistRole.PRODUCER)
}

class ArtistSong(id: EntityID<Int>) : IntEntity(id){
    companion object : IntEntityClass<ArtistSong>(ArtistsSongs)
    {
        override val dependsOnTables = (Artists innerJoin ArtistsSongs) innerJoin Songs

        override fun createInstance(entityId: EntityID<Int>, row: ResultRow?): ArtistSong {
            val tr = TransactionManager.current()
            row?.tryGet(Artists.id)?.let {
                Artist.wrap(it, row, tr)
            }
            row?.tryGet(Songs.id)?.let {
                Song.wrap(it, row, tr)
            }

            return super.createInstance(entityId, row)
        }
    }

    var song by Song referencedOn ArtistsSongs.songID
    var artist by Artist referencedOn ArtistsSongs.artist_ID
    var role by ArtistsSongs.role
}

enum class TagType{
    GENRE, MOOD, TEMPO, KEY, OTHER;
}

object Tags: IntIdTable(){
    val type = enumeration("type", TagType::class.java).default(TagType.OTHER)
    val label = varchar("publisher", 512)
}

class Tag(id: EntityID<Int>): Entity<Int>(id){
    companion object : EntityClass<Int, Tag>(Tags){
        fun getOrNew(label: String, type: TagType): Tag {
            return Tag.find { (Tags.label eq label) and (Tags.type eq type) }.firstOrNull()
                    ?: new {
                            this.label = label
                            this.type = type
                        }
        }
    }

    var label by Tags.label
    var type by Tags.type
    var songs by Song via SongTags

    operator fun component1() = label
    operator fun component2() = type

}

object SongTags : IntIdTable() {
    val songID = reference("songID", Songs)
    val tagID = reference("tagID", Tags)
}


class YoutubeVideo(id: EntityID<String>): Entity<String>(id){
    companion object : EntityClass<String, YoutubeVideo>(YoutubeTable)

    var song by Song referencedOn YoutubeTable.songID
}


val allTables = arrayOf(Songs, YoutubeTable, Artists, ArtistsSongs, Tags, SongTags)

fun <T> KMutableProperty0<SizedIterable<T>>.add(element: T){
    this.set(SizedCollection(get().plus(element)))
}
fun <T> KMutableProperty0<SizedCollection<T>>.remove(element: T){
    this.set(SizedCollection(get().minus(element)))
}