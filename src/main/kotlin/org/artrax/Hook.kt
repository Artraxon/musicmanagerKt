package org.artrax
import com.xenomachina.argparser.ArgParser
import org.apache.commons.lang3.SystemUtils
import org.jetbrains.exposed.sql.Database
import org.jetbrains.exposed.sql.SchemaUtils.create
import org.jetbrains.exposed.sql.transactions.transaction
import kotlin.properties.ReadOnlyProperty
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

fun main(args: Array<String>){
    pargs = args
    Database.connect("jdbc:h2:${GConf.dbpath}", driver = "org.h2.Driver")
    transaction {
        create(*allTables)
    }

}

var pargs : Array<String>? = null
object defaultConfLocations {
    val linux = "${System.getProperty("user.home")}/.config/Musicmanager"
    val windows = "${System.getenv("%LOCALAPPDATA%")}/Musicmanager"
    val detectedPath = when {
        SystemUtils.IS_OS_WINDOWS -> linux
        SystemUtils.IS_OS_LINUX -> linux
        else -> kotlin.run {
            println("Operating System not supported, using Settings for Linux")
            linux
        }
    }
}

class ParsedArgs() {
    val parser = ArgParser(pargs!!)
}

fun <T, U, R> ReadOnlyProperty<R, T>.transform(transformation: T.() -> U)
        = object : ReadOnlyProperty<R, U>{
            override fun getValue(thisRef: R, property: KProperty<*>): U
                    = this@transform.getValue(thisRef, property).transformation()
}

fun <T, U, R> ReadWriteProperty<R, T>.transform(readTransformation: T.() -> U, writeTransformation: U.() -> T)
        = object : ReadWriteProperty<R, U>{

            override fun getValue(thisRef: R, property: KProperty<*>)
                    = this@transform.getValue(thisRef, property).readTransformation()

            override fun setValue(thisRef: R, property: KProperty<*>, value: U)
                    = this@transform.setValue(thisRef, property, value.writeTransformation())
}