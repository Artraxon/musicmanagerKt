package org.artrax
import com.mpatric.mp3agic.ID3v24Tag
import com.mpatric.mp3agic.Mp3File
import org.artrax.ArtistRole.*
import org.artrax.sources.Source
import org.jetbrains.exposed.sql.SizedCollection
import org.jetbrains.exposed.sql.and
import org.jetbrains.exposed.sql.deleteWhere
import org.jetbrains.exposed.sql.select
import kotlin.reflect.KProperty0

/**
 * An instance of this class represents a org.artrax.Song and all related data.
 * All properties are retrieving the data from the sql database, so two instance of this class created with the same
 * [Song] are equivalent and can both be used in the same thread without problems
 */
class Element (private val song: Song){
    val name get() = song.name
    val artists get() = song.artists
    val artistRoles get() = song.artistsWithRoles.associate { it.artist to it.role }
    val tags: Map<String, TagType> get() = song.tags.associate { it.label to it.type }
    val sources get() = Source.sourceTables.filterNot { (_, it) -> it.select { it.songID eq song.id }.empty() }.keys
    val label get() = song.publisher

    val mp3File get() = Mp3File(song.location)
    val mp3Tag: ID3v24Tag get() {
        return try {
            mp3File.id3v2Tag as ID3v24Tag
        }catch (e: ClassCastException){
            error("${song.location} has an unsupported org.artrax.Tag format")
        }
    }

    /**
     * Used for separating the different cases of artists involvements
     * @param featured executed if the artist is [FEATURED]
     * @param single executed if there are only [PRODUCER] or only [PERFORMER]
     * @param both executed if there are both [PRODUCER] and [PERFORMER]
     */
    internal fun artistSwitch(artist: Artist, role: ArtistRole,
                              featured: (Artist, ArtistRole) -> Unit,
                              single: (Artist, ArtistRole) -> Unit,
                              both: (Artist, ArtistRole) -> Unit){
        if(role == FEATURED){
            featured(artist, role)
        }else if(artists.empty() ||
                //If the song only has one artist with the same role like the one added
                try{ artistRoles.entries.single().let { (PRODUCER to PERFORMER).contains(it) && it.value == role} }
                catch (e: Exception){false}){
            single(artist, role)
        }else if((role == PRODUCER && artistRoles.values.contains(PERFORMER))
                || role == PERFORMER && artistRoles.values.contains(PRODUCER) ) {
            //If both a performer and a producer will be present
            both(artist, role)
        }
    }

    private fun renewArtist(artist: Artist, oldplaces: Set<TagPlaces>, newPlaces: Set<TagPlaces>){
        oldplaces.cross(newPlaces).crossAction(
                both = { },
                receiver = { mp3Tag.asSetRemoveTag(artist.name, it) },
                crossed = { mp3Tag.asSetAddTag(artist.name, it) }
        )
    }

    fun addArtist(artist: Artist, vararg roles: ArtistRole = arrayOf(GConf.default_ArtistRole)) {
        with(ArtistMappings) {
            roles.sorted().forEach { artistRole ->
                //if this combination already exists, there is no need to execute anything below
                if (artistRoles.any { it.key == artist && it.value == artistRole }) return@forEach

                if (artistRole == REMIXER && artistRoles.containsValue(REMIXER).not()) {
                    artistRoles.forEach { artist, role ->
                        artistSwitch(artist, role,
                                featured = { artist, role ->
                                    renewArtist(artist, featured, Remix.featured)
                                },
                                single = { artist, role ->
                                    when (role) {
                                        PRODUCER -> renewArtist(artist, onlyProducer, Remix.onlyProducer)
                                        PERFORMER -> renewArtist(artist, onlyPerformer, Remix.onlyPerformer)
                                    }
                                },
                                both = { artist, role ->
                                    when (role) {
                                        PRODUCER -> renewArtist(artist, producer, Remix.producer)
                                        PERFORMER -> renewArtist(artist, performer, Remix.performer)
                                    }
                                })
                    }
                    mp3Tag.asSetAddTag(artist.name, *Remix.remixer.toTypedArray())
                } else {

                    fun add(property: KProperty0<Set<TagPlaces>>){
                        mp3Tag.asSetAddTag(artist.name, *selectPlace(property))
                    }
                    
                    artistSwitch(artist, artistRole,
                            featured = { artist, role -> add(this::featured) },
                            single = { artist, role ->
                                when (role) {
                                    PRODUCER -> add(this::onlyProducer)
                                    PERFORMER -> add(this::onlyPerformer)
                                }
                            },
                            both = { artist, role ->
                                when (role) {
                                    PRODUCER -> add(this::producer)
                                    PERFORMER -> add(this::performer)
                                }
                            })
                }
                ArtistSong.new {
                    this.song = song
                    this.artist = artist
                    this.role = artistRole
                }
            }
        }
    }

    /**
     * Determines the correct property to use.
     */
    private fun selectPlace(property: KProperty0<Set<TagPlaces>>): Array<TagPlaces>{
        with(ArtistMappings) {
            val whenRemix = mapOf(
                    this::onlyProducer to remix::onlyProducer,
                    this::onlyPerformer to remix::onlyPerformer,
                    this::performer to remix::performer,
                    this::producer to remix::producer,
                    this::featured to remix::featured
            )
            try {
                return (property.get().takeUnless { artistRoles.containsValue(REMIXER) }
                        ?: whenRemix[property]!!.get()).toTypedArray()
            } catch (e: Exception) {
                throw Exception("This property isn't mapped to a property")
            }
        }
    }

    fun removeArtist(artist: Artist){
        artistRoles.filter { it.key == artist }.forEach { artist, role -> removeArtistRole(artist, role) }
    }

    fun removeArtistRole(artist: Artist, vararg roles: ArtistRole){

        //SQL must be updated first, because the updated fields are needed for the artistSwitch function
        roles.sorted().forEach { role ->
            ArtistsSongs.deleteWhere {
                    ArtistsSongs.songID.eq(song.id) and
                    ArtistsSongs.artist_ID.eq(artist.id) and
                    ArtistsSongs.role.eq(role)
            }

            if(artistRoles.any { it.key == artist && it.value == role })return@forEach

            fun remove(property: KProperty0<Set<TagPlaces>>){
                mp3Tag.asSetRemoveTag(artist.name, *selectPlace(property))
            }

            with(ArtistMappings) {

                artistSwitch(artist, role,
                        featured = { artist, role -> remove(this::featured) },
                        single = { artist, role ->
                            when (role){
                                PERFORMER -> remove(this::onlyPerformer)
                                PRODUCER -> remove(this::onlyProducer)
                            }
                        },
                        both = { artist, role ->
                            when (role) {
                                PERFORMER -> remove(this::performer)
                                PRODUCER -> remove(this::producer)
                            }
                        })
                if (role == REMIXER) {
                    if (artistRoles.containsValue(REMIXER).not()) {
                        artistRoles.forEach { artist, role ->
                            artistSwitch(artist, role,
                                    featured = { artist, role -> renewArtist(artist, Remix.featured, featured) },
                                    single = { artist, role ->
                                        when (role){
                                            PERFORMER -> renewArtist(artist, Remix.onlyPerformer, onlyPerformer)
                                            PRODUCER -> renewArtist(artist, Remix.onlyProducer, onlyProducer)
                                        }
                                    },
                                    both = { artist, role ->
                                        when (role){
                                            PERFORMER -> renewArtist(artist, Remix.performer, performer)
                                            PRODUCER -> renewArtist(artist, Remix.producer, producer)
                                        }
                                    })
                        }
                    }
                    mp3Tag.asSetAddTag(artist.name, *Remix.remixer.toTypedArray())
                }
            }
        }
    }

    fun addTag(label: String, type: TagType) = addTag(Tag.getOrNew(label, type))
    fun addTag(tag: Tag){
        //A song can't contain multiple tags
        if (tag.songs.contains(song)) return

        tag.songs = SizedCollection(tag.songs.plus(song))

        mp3Tag.asSetAddTag(tag.label, SQLTagToMp3Tag[tag.type]!!)

    }

    fun setPublisher(publisher: String = "none"){
        mp3Tag.publisher = publisher
        song.publisher = Publisher.getOrNew(publisher)
    }


    fun removeTag(label: String, type: TagType) = removeTag(Tag.getOrNew(label, type))
    fun removeTag(tag: Tag){

        tag.songs = SizedCollection(tag.songs.minus(song))

        mp3Tag.asSetRemoveTag(tag.label, SQLTagToMp3Tag[tag.type]!!)
    }
}

private val SQLTagToMp3Tag = mapOf(
        TagType.TEMPO to TagPlaces.TEMPO,
        TagType.OTHER to TagPlaces.COMMENT,
        TagType.MOOD to TagPlaces.GROUPING,
        TagType.GENRE to TagPlaces.GENRE
)

fun <A> Pair<A, A>.contains(element: A): Boolean{
    return first == element || second == element
}
