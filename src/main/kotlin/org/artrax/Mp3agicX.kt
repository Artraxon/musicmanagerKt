package org.artrax
import com.mpatric.mp3agic.ID3v2
import org.artrax.TagPlaces.*
import kotlin.reflect.KMutableProperty0

var ID3v2.genreDescriptionX
    get() = this.genreDescription
    set(value) = this.setGenreDescription(value)

var ID3v2.commentX
    get() = this.comment
    set(value) = this.setComment(value)

var ID3v2.groupingX
    get() = this.grouping
    set(value) = this.setGrouping(value)

var ID3v2.keyX
    get() = this.key
    set(value) = this.setKey(value)

/**
 * You dont have to tell me that this is a dumb idea
 */
var ID3v2.bpmX
    get() = this.bpm.toString()
    set(value) = this.setBPM(value.toInt())

var ID3v2.artistX
    get() = this.artist
    set(value) = this.setArtist(value)

var ID3v2.composerX
    get() = this.composer
    set(value) = this.setComposer(value)

var ID3v2.originalArtistX
    get() = this.originalArtist
    set(value) = this.setOriginalArtist(value)

var ID3v2.albumArtistX
    get() = this.albumArtist
    set(value) = this.setAlbumArtist(value)

fun ID3v2.setArtist(place: TagPlaces, value: String){
    when (place){
        ARTIST -> artist = value
        COMPOSER -> composer = value
        ORIGINAL_ARTIST -> originalArtist = value
        ALBUM_ARTIST -> albumArtist = value
    }
}

val mp3TagToPlace= mapOf(
        ARTIST to ID3v2::artistX,
        COMPOSER to ID3v2::composerX,
        ORIGINAL_ARTIST to ID3v2::originalArtistX,
        ALBUM_ARTIST to ID3v2::albumArtistX,
        GENRE to ID3v2::genreDescriptionX,
        COMMENT to ID3v2::commentX,
        GROUPING to ID3v2::groupingX,
        TEMPO to ID3v2::bpmX,
        KEY to ID3v2::keyX
)

fun ID3v2.setTag(value: String, vararg places: TagPlaces){
    places.forEach { place ->
        mp3TagToPlace[place]!!.set(this, value)
    }

}


fun ID3v2.asSetAddTag(value: String, vararg places: TagPlaces){
    places.forEach { place ->
        mp3TagToPlace[place]!!.let {
            it.set(this, it.get(this).asSetAdd(value))
        }
    }
}


fun ID3v2.asSetRemoveTag(value: String, vararg places: TagPlaces){
    places.forEach { place ->
        mp3TagToPlace[place]!!.let {
            it.set(this, it.get(this).asSetRemove(value))
        }
    }
}

fun String.asSetRemove(toRemove: String) = split(GConf.delimiter).toSet().minus(toRemove).joinToString(GConf.delimiter)
fun String.asSetAdd(toAdd: String) = split(GConf.delimiter).toSet().plus(toAdd).joinToString(GConf.delimiter)
fun KMutableProperty0<String>.asSetRemove(toRemove: String) = set(get().asSetRemove(toRemove))
fun KMutableProperty0<String>.asSetAdd(toAdd: String) = set(get().asSetAdd(toAdd))
