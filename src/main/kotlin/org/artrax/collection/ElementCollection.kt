package org.artrax.collection
import org.artrax.Crossing
import org.artrax.Element
import org.artrax.Tag
import org.artrax.sources.Source
import java.util.*
import java.util.function.Consumer
import java.util.function.Predicate
import java.util.stream.Stream

abstract class ElementCollection<T : Source>
    private constructor(private val delegate: MutableCollection<Element>) : MutableCollection<Element> by delegate {
    constructor() : this(mutableListOf<Element>())

    abstract val associatedTag: Tag

    protected abstract fun addToRemote(element: Element): Boolean
    protected abstract fun removeFromRemote(element: Element): Boolean
    protected abstract fun localDifferenceToRemote(): Map<Crossing, Element>

    override fun add(element: Element): Boolean {
        return if(contains(element).not() && addToRemote(element)) {
            element.addTag(associatedTag)
            true
        }else false
    }

    override fun addAll(elements: Collection<Element>) = elements.any { add(it) }

    override fun removeIf(filter: Predicate<in Element>) = this.removeAll(this.filter(filter::test))

    override fun remove(element: Element): Boolean {
        return if(contains(element) && removeFromRemote(element)){
            element.removeTag(associatedTag)
            true
        }else false
    }

    override fun removeAll(elements: Collection<Element>) = elements.any { remove(it) }

    override fun retainAll(elements: Collection<Element>) = this.removeAll(this.minus(elements))

    override fun clear() {
        removeAll(this.toList())
    }

    override fun forEach(action: Consumer<in Element>?) {
       return iterator().forEach { action!!.accept(it) }
    }

    override fun stream(): Stream<Element> {
        TODO()
    }

    override fun parallelStream(): Stream<Element> {
        TODO()
    }

    override fun spliterator(): Spliterator<Element> {
        TODO()
    }


    /**
     * @return A [MutableIterator] for this element Collection.
     * This Iterator does not update when this instance is modified, but if an element is removed from the iterator [ElementCollection.remove] is called.
     */
    override fun iterator(): MutableIterator<Element> {
        val delegateIterator = delegate.iterator()
        return object : MutableIterator<Element> {
            val list = this@ElementCollection.toList()

            var currentIndex = -1
            override fun hasNext() = list.size > currentIndex + 1

            override fun next(): Element {
                check(hasNext())
                //Prefix since we are starting at -1
                return list[++currentIndex]
            }

            override fun remove() {
                check(currentIndex > -1 && hasNext())
                this@ElementCollection.remove(list[currentIndex])
            }
        }
    }
}
